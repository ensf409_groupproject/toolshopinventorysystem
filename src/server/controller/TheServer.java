/**
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.model.ToolShop;

/**
 * The class that communicates with the client and with the model
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class TheServer {
	/**
	 * the server socket
	 */
	private ServerSocket theServerSocket;
	/**
	 * the socket that accepts the client socket
	 */
	private Socket theSocket;
	/**
	 * The ToolShop object
	 */
	private ToolShop theShop;
	/**
	 * the bufferedreader that writes to theSocket
	 */
	private BufferedReader input;
	/**
	 * the printWriter that rights to the socket
	 */
	private PrintWriter output;

/**
 * constructs and object of type server	
 */
	protected TheServer() {
		theShop = new ToolShop();
		System.out.println("Server is running");
	}
/**
 * communicates with the Client application
 * @throws IOException
 */
	protected void runServer() throws IOException {
		String response;

		while (true) {
			theServerSocket = new ServerSocket(9456);
			theSocket = theServerSocket.accept();
			input = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
			output = new PrintWriter((theSocket.getOutputStream()), true);

			response = input.readLine();
			switch (response) {
			case "LIST":
				output.print(theShop.getInventory().getToolData().displayToolList());
				output.flush();
				break;
			case "SEARCH-NAME":
				String name = input.readLine();
				output.print(theShop.getInventory().getToolData().searchName(name));
				output.flush();
				break;
			case "SEARCH-ID":
				String idString = input.readLine();
				try {
					int id = Integer.parseInt(idString);
					output.print(theShop.getInventory().getToolData().searchID(id));
					output.flush();
				} catch (NumberFormatException e) {
					output.print("Tool not found");
					output.flush();
				}
				break;
			case "SELL":
				String[] quantityAndItem = input.readLine().split(" @ ");
				String instruction = theShop.attemptPurchase(quantityAndItem[1], Integer.parseInt(quantityAndItem[0]));
				if (instruction.equals("SUCCESS")) {
					output.print("SUCCESS");
				} else {
					output.println("FAILED");

					output.print(instruction);
				}
				output.flush();
				break;
			case "ADD":
				// add the tool
				String toolInfo = input.readLine();
				boolean success = theShop.getInventory().getToolData().insertTool(toolInfo);
				if (success) {
					output.print("SUCCESS");
				} else {
					output.print("FAILED");
				}
				output.flush();
				break;
			case "GENERATE-ORDER":
				output.println(theShop.printOrder());
			}

			input.close();
			output.close();
			theSocket.close();
			theServerSocket.close();

		}
	}

	public static void main(String[] args) {
		TheServer myServer = new TheServer();

		try {
			myServer.runServer();
		} catch (IOException ex) {
			System.out.println("Connection Error");
		} finally {
			try {
				if (myServer.input != null) {
					myServer.input.close();
				}
				if (myServer.output != null) {
					myServer.output.close();
				}
			} catch (IOException ex) {
				System.out.println("Error while closing");
			}

		}

	}

}

/**
 * stores an object of ReadFromFile that contains the tools and functions to access them
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

public class Inventory {
	/**
	 * the ReadFromFile object that accesses the data in the text file
	 */
	private ReadFromFile toolShopData;
	/**
	 * An Order object that
	 */
	private Order theOrder;

	/**
	 * constructs an object of type Inventory
	 */
	public Inventory() {
		toolShopData = new ReadFromFile();
		theOrder = new Order();
	}

	/**
	 * 
	 * @return toolShopData
	 */
	public ReadFromFile getToolData() {
		return toolShopData;
	}

	/**
	 * Calls a function in tool shop to decrease quantity and returns a String with
	 * information about whether a Tool's quantity could be decreased
	 * 
	 * @param name     the name of the tool
	 * @param quantity the quantity to be purchased
	 * @return instruction String containing information about whether the tool was
	 *         purchased
	 */
	public String sellTool(String name, int quantity) {
		Tool theTool = toolShopData.decreaseQuantity(name);
		String instruction = "TOOL NOT FOUND!";
		if (theTool != null) {
			instruction = theTool.decreaseItemQuantity(quantity);
			if (instruction.equals("ORDER MORE")) {
				theOrder.add(theTool.placeOrder());
			}
			if (instruction.equals("ALL GOOD") || instruction.equals("ORDER MORE")) {
				toolShopData.updateQuantity(name, quantity);
			}
		}

		return instruction;
	}

	/**
	 * Calls the function in order to write the order to a text file.
	 * 
	 * @return a String containing the status of the printing t file function
	 */
	public String createOrder() {
		return theOrder.printOrderToFile();
	}

}

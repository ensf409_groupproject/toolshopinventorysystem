/**
 * contains the information about orders the shop makes
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class Order {

	/**
	 * the date each order is made on
	 */
	private Calendar date;
	/**
	 * a list of all the orderlines in an order
	 */
	private ArrayList<OrderLine> order;
	/**
	 * the order id
	 */
	private int id;

	/**
	 * constructs an object of type Order
	 */
	public Order() {
		date = Calendar.getInstance();
		order = new ArrayList<OrderLine>();
		Random random = new Random();
		id = 10000 + random.nextInt(100000);
	}

	/**
	 * Adds orderline
	 * 
	 * @param an orderline to be added
	 */
	public void add(OrderLine placeOrder) {
		order.add(placeOrder);
	}

	/**
	 * Prints the current order to the text file
	 * 
	 * @return a String that contains the status of the file
	 */
	public String printOrderToFile() {
		String temp = "ORDER ID:			" + id + "\r\nDate Ordered:			" + date.getTime() + "\r\n";

		try (PrintWriter p = new PrintWriter(new BufferedWriter((new FileWriter((new File("orders.txt")), true))))) {
			p.append(temp);
			for (int i = 0; i < order.size(); i++) {
				p.append("\r\n");
				p.append(order.get(i).toString());
			}
			p.append("\r\n");
		} catch (IOException e) {
			e.printStackTrace();
			return "FAILED";

		}
		return "SUCCESS";
	}

}

package server.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This class will be replaced with the Database for Milestone 3 reads the tool
 * and supplier information from the text file
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */

public class ReadFromFile {
	/**
	 * the List of tools from the file
	 */
	private ArrayList<Tool> theToolList;
	/**
	 * the list of suppliers from the file
	 */
	private ArrayList<Supplier> theSupplierList;

	/**
	 * constructs an object of type ReadFromFile
	 */
	public ReadFromFile() {
		readToolList();
		readSupplierList();
	}

	/**
	 * reads the toolist from the file and adds it to an array list
	 */
	public void readToolList() {
		theToolList = new ArrayList<Tool>();
		try (Scanner sc = new Scanner(new File("items.txt"))) {

			while (sc.hasNext()) {
				String temp = sc.nextLine();
				String[] s = temp.split(";");
				int id = Integer.parseInt(s[0]);
				String name = s[1];
				int quantity = Integer.parseInt(s[2]);
				double price = Double.parseDouble(s[3]);
				int supId = Integer.parseInt(s[4]);
				Tool tool = new Tool(name, id, quantity, price, supId);
				theToolList.add(tool);

			}

		} catch (Exception e) {
			System.err.println("Items reading");
			System.err.println(e.getMessage());
		}
	}

	/**
	 * reads the supplier List from files and adds it to an array list
	 */
	public void readSupplierList() {
		theSupplierList = new ArrayList<Supplier>();
		try (Scanner sc = new Scanner(new File("suppliers.txt"))) {
			while (sc.hasNext()) {
				String temp = sc.nextLine();
				String[] s = temp.split(";");
				int id = Integer.parseInt(s[0]);
				String name = s[1];
				String address = s[2];
				String sales = s[3];
				Supplier supplier = new Supplier(id, name, address, sales);
				theSupplierList.add(supplier);
			}
		} catch (Exception f) {
			System.err.println(f.getMessage());
		}
	}

	/**
	 * constructs a string containing all the information from the items.txt file
	 * each tool information separated by " @ "
	 * 
	 * @return a String containing all the information from items.txt file
	 */
	public String displayToolList() {
		StringBuilder toolList = new StringBuilder();
		try (Scanner sc = new Scanner(new File("items.txt"))) {

			while (sc.hasNext()) {
				toolList.append(sc.nextLine());
				toolList.append(" @ ");

			}

		} catch (Exception e) {
			System.err.println("Items");
			System.err.println(e.getMessage());
		}
		return toolList.toString();
	}

	/**
	 * searches the file for the tool with the specified name
	 * 
	 * @param toolName the name of tool being searched
	 * @return a string with the tool information if the tool is found, or "Tool not
	 *         found" if it can't find the tool
	 */
	public String searchName(String toolName) {
		try (Scanner sc = new Scanner(new File("items.txt"))) {

			while (sc.hasNext()) {
				String temp = sc.nextLine();
				String[] s = temp.split(";");
				String name = s[1];
				if (name.equals(toolName)) {
					return temp;
				}

			}

		} catch (Exception e) {
			System.err.println("Items");
			System.err.println(e.getMessage());
		}
		return "Tool not found";
	}

	/**
	 * searches the file for the tool with the specified name
	 * 
	 * @param toolId the id of the tool being searched
	 * @return a string with the tool information if the tool is found, or "Tool not
	 *         found" if it can't find the tool
	 */
	public String searchID(int toolId) {
		try (Scanner sc = new Scanner(new File("items.txt"))) {
			while (sc.hasNext()) {
				String temp = sc.nextLine();
				String[] s = temp.split(";");
				int id = Integer.parseInt(s[0]);
				if (id == toolId) {
					return temp;
				}

			}

		} catch (Exception e) {
			System.err.println("Items");
			System.err.println(e.getMessage());
		}

		return "Tool not found";
	}

	/**
	 * searches the text file for the tool and constructs and object of type tool if
	 * found
	 * 
	 * @param name the name of the tool being decreased
	 * @return a Tool object if the tool is found or null if it is not found
	 */
	public Tool decreaseQuantity(String name) {
		String temp = searchName(name);
		if (!temp.equals("Tool not found")) {
			Tool toBeDecreased = createToolFromTextString(temp);
			return toBeDecreased;
		}

		return null;
	}

	/**
	 * creates a Tool from a String with ; delimiters separating infomation
	 * 
	 * @param textLine the String containing all of the tool information
	 * @return a Tool object created from the given string
	 */
	public Tool createToolFromTextString(String textLine) {
		String[] s = textLine.split(";");
		int id = Integer.parseInt(s[0]);
		String name = s[1];
		int quantity = Integer.parseInt(s[2]);
		double price = Double.parseDouble(s[3]);
		int supId = Integer.parseInt(s[4]);
		Tool tool = new Tool(name, id, quantity, price, supId);
		return tool;
	}

	/**
	 * checks to see if the tool being added is aready in the list, and if not it
	 * adds it too the list
	 * 
	 * @param toolInfo te name of the tool being added
	 * @return true if the tool is not in the list and is added, or false and tool
	 *         is not added if the tool is already in the list
	 */
	public boolean insertTool(String toolInfo) {
		try (BufferedWriter br = new BufferedWriter(new FileWriter(new File("items.txt"), true))) {
			Scanner sc = new Scanner(new File("items.txt"));
			String[] toolInfoAsAnArray = toolInfo.split(";");
			while (sc.hasNext()) {
				String[] fileStuff = sc.nextLine().split(";");
				if (fileStuff[1].contentEquals(toolInfoAsAnArray[1])
						|| fileStuff[0].contentEquals(toolInfoAsAnArray[0])) {
					return false;
				}
			}
			br.append(toolInfo + "\n");
		} catch (Exception f) {
			System.err.println(f.getMessage());
		}
		return true;
	}

	/**
	 * rewrites the items Text file with the updated quantity for the specified tool
	 * 
	 * @param name     the nam of the tool whose quantity is being changed
	 * @param quantity the amount the tool is changed by
	 */
	public void updateQuantity(String name, int quantity) {
		readToolList();
		try (BufferedWriter br = new BufferedWriter(new FileWriter(new File("items.txt")))) {
			for (Tool traverse : theToolList) {
				System.out.println(traverse);

				if (traverse.getToolName().equals(name)) {
					traverse.setQuantity(traverse.getQuantity() - quantity);
					System.out.println(traverse.getQuantity());
				}
				br.append(traverse.toString() + "\n");

			}

		} catch (Exception f) {
			System.err.println(f.getMessage());
		}

	}

	public static void main(String[] args) {
		ReadFromFile test = new ReadFromFile();
		System.out.println(test.displayToolList());
	}

}

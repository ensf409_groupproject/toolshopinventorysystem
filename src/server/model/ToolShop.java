/**
 * Contains all the information needed to run the tool shop
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

public class ToolShop {
	/**
	 * the toolshop inventory
	 */
	Inventory theInventory;

	/**
	 * constructs an object of type ToolShop
	 */
	public ToolShop() {
		theInventory = new Inventory();
		theInventory.toString();
	}

	/**
	 * 
	 * @return theInventory
	 */
	public Inventory getInventory() {
		return theInventory;
	}

	/**
	 * calls the inventory function sell Tool and processes and returns a different
	 * string depending on the success of the sellTool function
	 * 
	 * @param name     the name of the tool being purchased
	 * @param quantity the quantity being purchased
	 * @return a String containing "SUCCESS" if the tool quantity was decreased,
	 *         otherwise returns the instruction from the sellTool function
	 */
	public String attemptPurchase(String name, int quantity) {
		String instruction = theInventory.sellTool(name, quantity);
		if (instruction.equals("ALL GOOD") || instruction.equals("ORDER MORE")) {
			return "SUCCESS";
		} else {
			return instruction;
		}
	}

	/**
	 * calls the Inventory function that creates the order;
	 * 
	 * @return test
	 */
	public String printOrder() {
		String test = theInventory.createOrder();
		return test;

	}
}

/**
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

public class Tool {
	/**
	 * the id of the tool
	 */
	private int id;
	/**
	 * the name of the tool
	 */
	private String name;
	/**
	 * the quantity in stock of the tool
	 */
	private int quantity;
	/**
	 * the price of the tool
	 */
	private double price;
	/**
	 * the id of the supplier of the tool
	 */
	private int supplierId;

	/**
	 * constructs an object of type tool
	 * 
	 * @param name     the name of the tool
	 * @param id       the id of the tool
	 * @param quantity the quantity of the tool
	 * @param price    the price of the tool
	 * @param supId    the supplier id of the tool
	 */
	public Tool(String name, int id, int quantity, double price, int supId) {
		this.name = name;
		this.id = id;
		this.quantity = quantity;
		this.price = price;
		this.supplierId = supId;
	}

	/**
	 * checks to see if the tool quantity is able to be decreased and updates the
	 * quantity if possible
	 * 
	 * @param decQuant the amount the quantity is to be decreased by
	 * @return a String detailing whether the action was performed
	 */
	public String decreaseItemQuantity(int decQuant) {
		if (quantity < decQuant) {
			return "ONLY " + quantity + " IN STOCK.";
		}
		quantity -= decQuant;
		if (quantity < 40) {

			return "ORDER MORE";
		}
		return "ALL GOOD";
	}

	/**
	 * generates an orderline for a tool with the quantity needed to be ordered
	 * 
	 * @param quantity the quantity to be ordered
	 * @return an OrderLine object
	 */

	public OrderLine placeOrder() {
		OrderLine theLine = new OrderLine(this, 50 - quantity);
		return theLine;
	}

	/**
	 * converts the tool fields to a String delimited by ";"
	 */
	public String toString() {
		return id + ";" + name + ";" + quantity + ";" + price + ";" + supplierId;
	}

	/**
	 * returns the tool Name
	 * 
	 * @return name
	 */
	public String getToolName() {
		// TODO Auto-generated method stub
		return name;
	}

	/**
	 * returns the tool ID
	 * 
	 * @return id
	 */
	public int getToolId() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * returns the tool quantity
	 * 
	 * @return quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * sets the quantity
	 * 
	 * @param changeAmount the amount the quantity is changed by
	 */
	public void setQuantity(int changeAmount) {
		quantity = changeAmount;
	}
}

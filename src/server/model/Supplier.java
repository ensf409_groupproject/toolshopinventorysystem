/**
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package server.model;

import java.util.ArrayList;

public class Supplier {
	/**
	 * the supplier id
	 */
	private int id;
	/**
	 * the name of the supplier
	 */
	private String name;
	/**
	 * the address of the supplier
	 */
	private String address;
	/**
	 * the sales contact of the supplier
	 */
	private String salesContact;

	/**
	 * constructs an object of type supplier
	 * 
	 * @param id      the id of the suppplier
	 * @param name    the name of the supplier
	 * @param address the address of the supplier
	 * @param sales   the name of the supplier sales'contact
	 */
	public Supplier(int id, String name, String address, String sales) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.salesContact = sales;
	}

}

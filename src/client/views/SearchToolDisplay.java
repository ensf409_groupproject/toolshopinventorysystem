/**
 * Contains data fields and methods to create and display the search tool window of the
 * Tool Shop application
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */package client.views;

import java.awt.Container;
import javax.swing.*;

public class SearchToolDisplay extends JFrame {
	/**
	 * constructs an object of type SearchToolDisplay
	 */
	public SearchToolDisplay() {
		super();

		initComponents();

		this.setVisible(false);
		this.setSize(500, 200);
		this.setResizable(false);
	}
  
	/**
	 * the area where the tool info will be displayed
	 */
	private JTextArea theToolInfo;
	
	/**
	 * the area where the input will be inserted
	 */
	private JTextField input;
	
	/**
	 * Text fields and buttons for the search tool display that correspond to the purposes search by ID, 
	 * and search by name
	 */
	private JButton searchById;
	private JButton searchByName;

	/**
	 *  Initializes buttons and panels for the search tool view
	 */
	private void initComponents() {
		theToolInfo = new JTextArea("", 10, 15);
		theToolInfo.setEditable(false);

		input = new JTextField("", 15);
		searchById = new JButton("Search by tool ID");
		searchByName = new JButton("Search by tool Name");

		Container c = this.getContentPane();
		JPanel thePanel = new JPanel();

		thePanel.add(searchById);
		thePanel.add(searchByName);

		c.add(thePanel, "East");

		thePanel = new JPanel();
		thePanel.add(input);
		thePanel.add(theToolInfo);

		c.add(thePanel, "Center");
	}
	/**
	 * returns the Search by Name Button for the frame
	 * @return searchByName
	 */
	public JButton getSearchByNameButton() {
		return searchByName;
	}
	/**
	 * returns the Search by ID Button for the frame
	 * @return searchById
	 */
	public JButton getSearchByIdButton() {
		return searchById;
	}
	/**
	 * returns the input of Input field
	 * @return input
	 */
	public JTextField getInput() {
		return input;
	}
	
	/**
	 * This method create an string of tool info if it can find the tool
	 * @param info
	 */
	public void setToolInfo(String info) {
    		if (!info.equals("Tool not found")) {
			String[] temp = info.split(";");
			theToolInfo.setText("Name: " + temp[1] + "\nID: " + temp[0] + "\nQuanity: " + temp[2] + "\nPrice: "
					+ temp[3] + "\nSupplier ID: " + temp[4]);
		} else {
			JOptionPane.showMessageDialog(new JFrame(), "Tool not found", ":\\", JOptionPane.ERROR_MESSAGE);
		}
	}
	/**
	 * This method clear entries of input text field
	 */
	public void clearEntries() {
		input.setText("");

	}

}

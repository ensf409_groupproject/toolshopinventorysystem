/**
 * Contains data fields and methods to create and display the main window of the
 * Tool Shop application
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package client.views;

import java.awt.Container;
import java.awt.Dimension;
import javax.swing.*;

public class MainViewDisplay extends JFrame {
	
	/**
	 * buttons for the main display that correspond to the purposes Search Tool,
	 * Add Tool, Sell, Refresh, and Generate Order 
	 */
	
	private JButton searchTool;
	private JButton addTool;
	private JButton sell;
	private JButton refresh;
	private JButton generateOrder;

	/**
	 * the area where the items will be displayed
	 */
	public JList theToolList;
	private String[] theTools = { "Hit refresh" };
	
	/**
	 * constructs an object of type MainViewDisplay
	 */
	public MainViewDisplay() {
		super();
		initComponents();
	}
	/**
	 *  Initializes buttons and panels for the main view display
	 */
	private void initComponents() {
		sell = new JButton("Sell Tool");
		addTool = new JButton("Add Tool");
		searchTool = new JButton("Search Tools");
		refresh = new JButton("Refresh");
		generateOrder=new JButton("Generate Order");

		theToolList = new JList(theTools);
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportView(theToolList);
		scrollPane.setPreferredSize(new Dimension(400, 400));

		Container c = this.getContentPane();

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setResizable(false);

		JPanel myPanel = new JPanel();

                myPanel.add(new JLabel("Items"));
                c.add(myPanel, "North");
                
                myPanel = new JPanel();
		myPanel.add(scrollPane);
		c.add(myPanel, "Center");

		myPanel = new JPanel();
		myPanel.add(addTool);
		myPanel.add(searchTool);
		myPanel.add(refresh);
		myPanel.add(sell);
		myPanel.add(generateOrder);
		c.add(myPanel, "South");

		this.setSize(500, 500);
		this.setVisible(true);
	}
	/**
	 * Returns the "Refresh" button for the frame
	 * @return refresh
	 */
	public JButton getRefresh() {
		return refresh;
	}
	/**
	 * Returns the "Search Tool" button for the frame
	 * @return searchTool
	 */
	public JButton getSearchButton() {
		return searchTool;
	}
	/**
	 * Returns the "Sell" button for the frame
	 * @return sell
	 */
	public JButton getSellButton() {
		return sell;
	}
	/**
	 * Sets the Item
	 * @param theItems
	 */
	public void setItems(String[] theItems) {
		theToolList.setListData(theItems);
        
	}
    /**
     * Sets the tools
     * @param theTools
     */
    public void setTheTools(String[] theTools){
        this.theTools = theTools;
    }
    /**
     * Returns the tools 
     * @return theTools
     */
    public String[] getTheTools(){
        return theTools;
    }
    /**
     * Returns the "Add Tool" button for the frame
     * @return addTool
     */
	public JButton getAddTool() {
		return addTool;

	}
    /**
     * Returns a list of tools to the frame
     * @return theToolList
     */
    public JList getList(){
        return theToolList;
    }
    /**
     * Returns the "Generate Order" button for the frame
     * @return generateOrder
     */
	public JButton getGenerateOrder() {
		return generateOrder;
	}
}

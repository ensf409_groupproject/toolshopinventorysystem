/**
 * Contains data fields and methods to create and display the sell tool window of the
 * Tool Shop application
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */

package client.views;

import java.awt.Container;
import java.awt.Dimension;
import javax.swing.*;

public class DecreaseToolDisplay extends JFrame {

	/**
	 * constructs an object of type DecreaseToolDisplay
	 */
	public DecreaseToolDisplay() {
		super();

		initComponents();

		this.setVisible(false);
		this.setSize(500, 200);
		this.setResizable(false);
	}
	
	/**
	 * the area where the tool name will be displayed
	 */
	private JTextField toolName;
	
	/**
	 * the area where the the quantity can be selected
	 */
	private JSpinner quantity;
	
	/**
	 * by pressing the sell button the item selected will be decreased
	 */
	private JButton sell;
	
	/**
	 *  Initializes buttons and panels for the sell tool view 
	 */
	private void initComponents() {
		toolName = new JTextField("", 15);
                SpinnerNumberModel sm = new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1);
		quantity = new JSpinner(sm);
		quantity.setPreferredSize(new Dimension(45, 20));
		sell = new JButton("Sell");

		Container c = this.getContentPane();
		JPanel thePanel = new JPanel();
		thePanel.add(new JLabel("Tool Name"));
		thePanel.add(toolName);
		thePanel.add(new JLabel("Quantity"));
		thePanel.add(quantity);
		c.add(thePanel, "Center");

		thePanel = new JPanel();
		thePanel.add(sell);
		c.add(thePanel, "South");
	}

	/**
	 * Returns the "Sell" button to the frame
	 * @return sell
	 */
	public JButton getSellButton() {
		return sell;
	}

	/**
	 * Returns the quantity of tools
	 * @return quantity
	 */
	public JSpinner getQuantity() {
		return quantity;
	}
	
	/**
	 * Returns the input of the tool name
	 * @return toolName
	 */
	public JTextField getInput() {
		return toolName;
	}
	/**
	 * This method clear entries of text fields
	 */
	public void clearEntries() {
		toolName.setText("");
	}

}

package client.controller;

import client.views.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *  This class controls the process of selling tools
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class DecreaseToolController {

    DecreaseToolDisplay theDecreaseToolDisplay;
    Socket theSocket;

    /**
     * constructs an object of type DecreaseToolController
     *
     * @param theDecreaseToolDisplay the Frame for decreasing a tool
     */
    public DecreaseToolController(DecreaseToolDisplay theDecreaseToolDisplay) {
        this.theDecreaseToolDisplay = theDecreaseToolDisplay;
        initController();
    }

    /**
     * The following class called SellTool decreases items by entering tool name
     * and quantity of the items
     */
    public class SellTool implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            int quantity = (int) theDecreaseToolDisplay.getQuantity().getValue();
            String theItemName = theDecreaseToolDisplay.getInput().getText();
            if (theItemName.isEmpty()) {
                JOptionPane.showMessageDialog(new JFrame(), "Enter a tool name", "(:O)",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {

                theSocket = new Socket("localhost", 9456);
                PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
                pw.println("SELL");
                pw.println(quantity + " @ " + theItemName);
                BufferedReader br = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));

                String response = br.readLine();
                switch (response) {

                    case "SUCCESS":
                        showSuccess(quantity, theItemName);
                        break;
                    case "FAILED":
                        showFail(br.readLine());
                        break;

                }

            } catch (IOException ex) {
                System.out.println("Connection error");
            } finally {
                try {
                    theSocket.close();
                } catch (IOException ex) {
                    Logger.getLogger(DecreaseToolController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            theDecreaseToolDisplay.clearEntries();
        }

        /**
         * This method shows a message if the process of selling is successful
         *
         * @param quantity
         * @param toolName
         */
        private void showSuccess(int quantity, String toolName) {
            JOptionPane.showMessageDialog(new JFrame(), "Successfully sold " + quantity + " of " + toolName);
        }

        /**
         * This method shows a message if the process of selling is not
         * successful
         *
         * @param message
         */
        private void showFail(String message) {
            JOptionPane.showMessageDialog(new JFrame(), "Couldn't sell tool: " + message, "(D:)",
                    JOptionPane.ERROR_MESSAGE);
        }

    }

    /**
     * Initializes the controller for the GUI.
     */
    private void initController() {
        theDecreaseToolDisplay.getSellButton().addActionListener(new SellTool());
    }
}

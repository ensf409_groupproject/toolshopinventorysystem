/**
 * This class is made up of nested classes that contain the ActionListeners for
 * every object in the FrontEnd
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
package client.controller;

import client.controller.MainViewController.GenerateOrder;
import client.views.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MainViewController {

    private Socket theSocket;
    private MainViewDisplay theMainViewDisplay;
    private SearchToolDisplay theSearchToolDisplay;
    private InsertToolDisplay theInsertToolDisplay;
    private DecreaseToolDisplay theDecreaseToolDisplay;

    /**
     * constructs an object of type MainViewController
     *
     * @param theMainViewDisplay the MainViewDisplay
     * @param theSearchToolDisplay the SearchToolDisplay
     * @param theInsertToolDisplay the InsertToolDisplay
     * @param theDecreaseToolDisplay the DecreaseToolDisplay
     */
    public MainViewController(MainViewDisplay theMainViewDisplay, SearchToolDisplay theSearchToolDisplay,
            InsertToolDisplay theInsertToolDisplay, DecreaseToolDisplay theDecreaseToolDisplay) {

        this.theMainViewDisplay = theMainViewDisplay;
        this.theSearchToolDisplay = theSearchToolDisplay;
        this.theInsertToolDisplay = theInsertToolDisplay;
        this.theDecreaseToolDisplay = theDecreaseToolDisplay;

        initComponents();
        initToolList();
    }

    /**
     * Listens for the Sell tool button to be pressed and then open another
     * window for selling items
     *
     */
    public class GoToDecreaseItem implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            theSearchToolDisplay.setVisible(false);
            theInsertToolDisplay.setVisible(false);
            theDecreaseToolDisplay.setVisible(true);
        }

    }

    /**
     * Listens for the Generate Order button to be pressed
     *
     */
    public class GenerateOrder implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {

                theSocket = new Socket("localhost", 9456);
                PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
                pw.println("GENERATE-ORDER");
                pw.println(theSearchToolDisplay.getInput().getText());
                BufferedReader br = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
                String response = br.readLine();

                switch (response) {

                    case "SUCCESS":
                        JOptionPane.showMessageDialog(new JFrame(), "Order added to orders.txt", ":)",
                                JOptionPane.PLAIN_MESSAGE);
                        break;
                    case "FAILED":
                        JOptionPane.showMessageDialog(new JFrame(), "Unable to generate order", ":(",
                                JOptionPane.PLAIN_MESSAGE);
                        break;

                }

            } catch (IOException ex) {
                System.out.println("Connection error");
            } finally {

                try {
                    theSocket.close();
                } catch (IOException ex) {
                    Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

    }

    /**
     *
     * Listens for the Add tool button to be pressed and then open another
     * window for adding items
     *
     */
    public class GoToInsertTool implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            theInsertToolDisplay.setVisible(true);
            theDecreaseToolDisplay.setVisible(false);
            theSearchToolDisplay.setVisible(false);
        }

    }

    /**
     *
     * Listens for the Search tools button to be pressed and then open another
     * window for searching items
     *
     */
    public class GoToSearchTool implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent arg0) {
            theDecreaseToolDisplay.setVisible(false);
            theInsertToolDisplay.setVisible(false);
            theSearchToolDisplay.setVisible(true);

            if (!theMainViewDisplay.getList().isSelectionEmpty()) {

                theSearchToolDisplay
                        .setToolInfo(theMainViewDisplay.getTheTools()[theMainViewDisplay.getList().getSelectedIndex()]);

            }
        }

    }

    /**
     *
     * Listens for the Refresh button to be pressed and then it refreshes the
     * list
     *
     */
    public class RefreshList implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            initToolList();
        }
    }

    /**
     * Updates the Tool List with information from the server
     */
    private void initToolList() {
        try {

            theSocket = new Socket("localhost", 9456);
            PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
            pw.println("LIST");
            BufferedReader br = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
            String theItems = br.readLine();
            theMainViewDisplay.setTheTools(theItems.split(" @ "));
            // theItems = theItems.replace(";", " ");
            String[] theItemsSplit = theItems.split(" @ ");
            for (int i = 0; i < theItemsSplit.length; i++) {
                theItemsSplit[i] = theItemsSplit[i].substring(theItemsSplit[i].indexOf(";") + 1,
                        findSecondIndex(theItemsSplit[i], ";"));
            }

            theMainViewDisplay.setItems(theItemsSplit);

        } catch (IOException ex) {
            System.out.println("Connection error");
        } finally {
            try {
                if (theSocket != null) {
                    theSocket.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Returns the index of the second occurence of string b in string a
     *
     * @param a the String being checked
     * @param b the String being checked for the second occurence of
     * @return index of the 2nd occurence, -1 if not found;s
     */
    private int findSecondIndex(String a, String b) {
        int count = 0;
        for (int i = 0; i < a.length(); i++) {
            if (a.substring(i, i + b.length()).equals(b)) {
                count++;
            }
            if (count == 2) {
                return i;
            }

        }
        return -1;
    }

    /**
     * Initializes the controller for the GUI. That is, it "wires" up all
     * buttons to their respective actions.
     */
    private void initComponents() {
        theMainViewDisplay.getRefresh().addActionListener(new RefreshList());
        theMainViewDisplay.getSearchButton().addActionListener(new GoToSearchTool());
        theMainViewDisplay.getAddTool().addActionListener(new GoToInsertTool());
        theMainViewDisplay.getSellButton().addActionListener(new GoToDecreaseItem());
        theMainViewDisplay.getGenerateOrder().addActionListener(new GenerateOrder());
    }

}

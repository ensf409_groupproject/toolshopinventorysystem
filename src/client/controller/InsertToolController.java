package client.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import client.controller.SearchToolController.SearchByName;
import client.views.InsertToolDisplay;
import client.views.SearchToolDisplay;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Controller for the Insert Tool Display
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class InsertToolController {

    private InsertToolDisplay theInsertToolDisplay;

    /**
     * Constructs a new InsertToolDisplay and initializes all ActionListers for
     * the buttons
     *
     * @param theInsertToolDisplay
     */
    public InsertToolController(InsertToolDisplay theInsertToolDisplay) {
        this.theInsertToolDisplay = theInsertToolDisplay;
        initController();

    }

    /**
     * Adds a tool to the Item list on the Server. If the input is invalid, an
     * error frame is outputted. If the tool on the server side cannot be added,
     * an error frame is outputted. If the tool on the server side is added, a
     * success frame is outputted
     */
    public class AddTool implements ActionListener {

        @Override

        public void actionPerformed(ActionEvent e) {

            String name;
            int id, supplierId, quantity;
            double price;

            if (theInsertToolDisplay.getInputName().getText().isEmpty()
                    || theInsertToolDisplay.getInputQuantity().getText().isEmpty()
                    || theInsertToolDisplay.getInputPrice().getText().isEmpty()
                    || theInsertToolDisplay.getInputSupplierId().getText().isEmpty()) {
                JOptionPane.showMessageDialog(new JFrame(), "Please fill out all fields.", "(-_-)",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                name = theInsertToolDisplay.getInputName().getText();
                quantity = Integer.parseInt(theInsertToolDisplay.getInputQuantity().getText());
                price = Double.parseDouble(theInsertToolDisplay.getInputPrice().getText());
                supplierId = Integer.parseInt(theInsertToolDisplay.getInputSupplierId().getText());
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(new JFrame(), "Please make sure fields are correct.", ":'(",
                        JOptionPane.ERROR_MESSAGE);

                return;
            }
            try (Socket theSocket = new Socket("localhost", 9456)) {
                id = Integer.parseInt(theInsertToolDisplay.getInputID().getText());

                name = theInsertToolDisplay.getInputName().getText();
                quantity = Integer.parseInt(theInsertToolDisplay.getInputQuantity().getText());
                price = Double.parseDouble(theInsertToolDisplay.getInputPrice().getText());
                supplierId = Integer.parseInt(theInsertToolDisplay.getInputSupplierId().getText());

                PrintWriter pw = new PrintWriter(theSocket.getOutputStream(), true);
                BufferedReader rb = new BufferedReader(new InputStreamReader(theSocket.getInputStream()));
                pw.println("ADD");
                pw.println(id + ";" + name + ";" + quantity + ";" + price + ";" + supplierId);

                String response = rb.readLine();
                switch (response) {

                    case "SUCCESS":
                        JOptionPane.showMessageDialog(new JFrame(), "Tool added successfully", ":)",
                                JOptionPane.PLAIN_MESSAGE);
                        break;
                    case "FAILED":
                        JOptionPane.showMessageDialog(new JFrame(), "Tool could not be added", ":(",
                                JOptionPane.PLAIN_MESSAGE);
                        break;
                }

            } catch (IOException ex) {
                System.out.println("Connection error");
            }

            theInsertToolDisplay.setVisible(false);
            theInsertToolDisplay.clearEntries();
        }
    }

    /**
     * Initializes all ActionListeners for the InsertToolDisplay buttons
     */
    private void initController() {
        theInsertToolDisplay.getAddButton().addActionListener(new AddTool());
    }

}

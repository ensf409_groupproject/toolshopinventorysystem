package client.controller;

import client.views.*;

/**
 *
 * This class creates an objects for all the displays and controllers This class
 * contains the main
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class TheClient {

    public static void main(String[] args) {

        SearchToolDisplay theSearchToolDisplay = new SearchToolDisplay();
        SearchToolController theSearchToolController = new SearchToolController(theSearchToolDisplay);

        DecreaseToolDisplay theDecreaseToolDisplay = new DecreaseToolDisplay();
        DecreaseToolController theDecreaseToolController = new DecreaseToolController(theDecreaseToolDisplay);

        InsertToolDisplay theInsertToolDisplay = new InsertToolDisplay();
        InsertToolController theInsertToolController = new InsertToolController(theInsertToolDisplay);

        MainViewDisplay theMainViewDisplay = new MainViewDisplay();
        MainViewController theMainViewController = new MainViewController(theMainViewDisplay, theSearchToolDisplay,
                theInsertToolDisplay, theDecreaseToolDisplay);

    }

}
